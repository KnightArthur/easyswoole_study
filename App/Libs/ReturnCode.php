<?php


namespace App\Libs;


class ReturnCode
{
    const SUCCESS = 0;
    const SYSTEM_FAIL = 500;
    const WRONG_PWD = 1001;
    const USER_NOT_FOUND = 1002;
    const USER_NOT_LOGIN = 1003;

    //中文错误详情
    public static $codeTexts = [
        0 => '操作成功',
        400 => '未登录',
        401 => '登陆失效',
        500 => '系统错误',
        1001 => '密码错误',
        1002 => '用户不存在',
        1003 => '请先登陆'

    ];


    public static function success($data = null, $msg = '')
    {
        if (empty($msg) && isset(self::$codeTexts[self::SUCCESS])) {
            $msg = self::$codeTexts[self::SUCCESS];
        }
        return ['code' => self::SUCCESS, 'msg' => $msg, 'data' => $data];
    }

    public static function error($code, $msg = '', $data = [])
    {
        if (empty($msg) && isset(self::$codeTexts[$code])) {
            $msg = self::$codeTexts[$code];
        }

        if ($code == ReturnCode::SUCCESS) {
            $code = ReturnCode::SYSTEM_FAIL;
            $msg = '系统错误';
        }

        return ['code' => $code, 'msg' => $msg, 'data' => []];
    }


}