<?php

namespace App\HttpController;

use App\Libs\ReturnCode;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\HttpAnnotation\AnnotationController;
use EasySwoole\Session\Context;

class BaseController extends AnnotationController
{

    public function initialize()
    {
    }

    protected function session(): ?Context
    {
        // 封装一个方法，方便我们快速获取 session context
        $sessionId = $this->request()->getAttribute('easy_session');
        return \App\Tools\Session::getInstance()->create($sessionId);
    }

    public function index()
    {
        $this->actionNotFound('index');
    }

    /**
     * 获取用户的真实IP
     * @param string $headerName 代理服务器传递的标头名称
     * @return string
     */
    protected function clientRealIP($headerName = 'x-real-ip')
    {
        $server = ServerManager::getInstance()->getSwooleServer();
        $client = $server->getClientInfo($this->request()->getSwooleRequest()->fd);
        $clientAddress = $client['remote_ip'];
        $xri = $this->request()->getHeader($headerName);
        $xff = $this->request()->getHeader('x-forwarded-for');
        if ($clientAddress === '127.0.0.1') {
            if (!empty($xri)) {  // 如果有 xri 则判定为前端有 NGINX 等代理
                $clientAddress = $xri[0];
            } elseif (!empty($xff)) {  // 如果不存在 xri 则继续判断 xff
                $list = explode(',', $xff[0]);
                if (isset($list[0])) $clientAddress = $list[0];
            }
        }
        return $clientAddress;
    }

    protected function input($name, $default = null)
    {
        $post_param = json_decode($this->request()->getBody()->__toString(), true);
        if (!$name) {
            $post_value = $post_param;
        } else {
            if (isset($post_param[$name])) {
                $post_value = $post_param[$name];
            } else {
                $post_value = null;
            }
        }
        $value = $this->request()->getRequestParam($name);
        return $post_value ? $post_value : ($value ? $value : '');
    }

    protected function success($data = [], $msg = '请求成功')
    {
        $this->writeJson(200, $data, $msg);
    }

    protected function error($code = 500, $msg = '', $data = [])
    {
        echo json_encode($msg);
        if (empty($msg) && isset(ReturnCode::$codeTexts[$code])) {
            $msg = ReturnCode::$codeTexts[$code];
        }
        $this->writeJson($code, $data, $msg);
    }
}