<?php


namespace App\HttpController\Api;


use EasySwoole\Http\AbstractInterface\AbstractRouter;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use FastRoute\RouteCollector;

class Router extends AbstractRouter
{
    function initialize(RouteCollector $routeCollector)
    {
        /*
          * eg path : /router/index.html  ; /router/ ;  /router
         */
        $routeCollector->addGroup('/api/v202101', function (\FastRoute\RouteCollector $collector) {

            $collector->post('/login','/Api/UserController/login');
        });
//        $routeCollector->get('/api/v202101/user','/Api/UserController/index');
//        $routeCollector->get('/api/v202101/user/{id:\d+}','/Api/UserController/show');
//        $routeCollector->post('/api/v202101/user','/Api/UserController/getUserList');
//        $routeCollector->get('/api/v202101/user','/Api/UserController/getUserList');
//        $routeCollector->get('/api/v202101/user','/Api/UserController/getUserList');
        /*
         * eg path : /closure/index.html  ; /closure/ ;  /closure
         */
        $routeCollector->get('/closure',function (Request $request,Response $response){
            $response->write('this is closure router');
            //不再进入控制器解析
            return false;
        });
    }
}