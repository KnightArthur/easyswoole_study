<?php


namespace App\HttpController\Api;


use App\Libs\ReturnCode;
use App\Model\User;
use App\Model\User1;
use EasySwoole\Component\Context\ContextManager;

class UserController extends CommonController
{
    protected $no_login = ['login'];

    public function login()
    {
        $mobile = $this->input('mobile', '');
        $password = $this->input('password', '');
        $user = new User();
        $user->mobile = $mobile;
//        $user->password = $password;
        $user_info = $user->login();
        if (!$user_info) {
            //用户不存在的情况下
            $user_info = $user->register();
            $user->id = $user_info['id'];
            $user_info = $user->login();
        } else {
            $pwd_verify = password_verify($password, $user_info['password']);
            echo json_encode($pwd_verify);
            if (!$pwd_verify) {
                $this->error(ReturnCode::WRONG_PWD);
                return;
            }
        }
        $session_id = md5(time() . "user_id" . $user_info['id']);


        unset($user_info['password']);
        $user_info['session_id'] = $session_id;
        $user_info['auth_key'] = md5($session_id . $user_info['id'] . time());
        $redis = new \EasySwoole\Redis\Redis(new \EasySwoole\Redis\Config\RedisConfig([
            'host' => '47.105.150.191',
            'port' => '6379',
            'auth' => 'jiali0222',
            'password' => 'jiali0222',
            'serialize' => \EasySwoole\Redis\Config\RedisConfig::SERIALIZE_NONE
        ]));
        $redis->set($session_id, $user_info);
        $this->session()->set('user_info', $user_info);
        $this->success(['user_info' => $user_info]);
    }
}