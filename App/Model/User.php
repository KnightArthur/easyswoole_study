<?php

declare (strict_types=1);

namespace App\Model;

use EasySwoole\HyperfOrm\Model;
use EasySwoole\Mysqli\QueryBuilder;

/**
 * @property string $demo_id
 * @property int $create_at
 * @property int $update_at
 */
class User extends Model
{
    /**
     * primaryKey
     *
     * @var string
     */
    protected $primaryKey = 'id ';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'create_at', 'update_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'create_at' => 'datetime', 'update_at' => 'datetime'];



    public function login()
    {

        $username = $this->username;
        $mobile = $this->mobile;
        $info = self::where(function ($q) use ($username, $mobile) {
            if ($username) {
                $q->where('username', $username);
            }
            if ($mobile) {
                $q->where('mobile', $mobile);
            }
        })->first();

        return $info;
    }


    /**
     * 注册
     * @return User
     * @throws \EasySwoole\ORM\Exception\Exception
     */
    public function register(): ?User
    {
        $username = $this->username;
        $mobile = $this->mobile;
        $password = $this->password;
        $obj = array();
        if ($username) {
            $obj['username'] = $username;
        }
        if ($mobile) {
            $obj['mobile'] = $mobile;
        }
        if ($password) {
            $obj['password'] = password_hash($password, PASSWORD_BCRYPT);
        }
        $user_info = self::create($obj);
        return $user_info;
    }

}