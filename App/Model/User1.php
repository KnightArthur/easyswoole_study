<?php


namespace App\Model;


use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\ORM\AbstractModel;

class User1 extends AbstractModel
{

    protected $tableName = 'fa_user';
    protected $primaryKey = 'id';

    protected $autoTimeStamp = 'datetime';
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';

    /**
     * 登陆
     * @return User1
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function login(): ?User1
    {
        $username = $this->username;
        $mobile = $this->mobile;
        $info = self::create()->get(function (QueryBuilder $q) use ($username, $mobile) {
            if ($username) {
                $q->where('username', $username);
            }
            if ($mobile) {
                $q->where('mobile', $mobile);
            }
        });

        return $info;
    }

    /**
     * 注册
     * @return User1
     * @throws \EasySwoole\ORM\Exception\Exception
     */
    public function register(): ?User1
    {
        $username = $this->username;
        $mobile = $this->mobile;
        $password = $this->password;
        $obj = array();
        if ($username) {
            $obj['username'] = $username;
        }
        if ($mobile) {
            $obj['mobile'] = $mobile;
        }
        if ($password) {
            $obj['password'] = password_hash($password, PASSWORD_BCRYPT);
        }
        $user_info = self::create($obj);
        $user_info->save();
        return $user_info;
    }

    public function getOne()
    {
        $id = $this->id;
        $info = self::create()->get($id);
        return $info;
    }
}