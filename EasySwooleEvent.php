<?php


namespace EasySwoole\EasySwoole;


use App\Parser\WebSocketParser;
use App\Tools\Session;
use EasySwoole\Component\Di;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\HyperfOrm\ConfigFactory;
use EasySwoole\HyperfOrm\ConnectionResolver;
use EasySwoole\HyperfOrm\Container;
use EasySwoole\HyperfOrm\MysqlPool;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\DbManager;
use EasySwoole\Pool\Manager;
use EasySwoole\Session\FileSession;
use EasySwoole\Utility\Random;
use Psr\Container\ContainerInterface;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Database\ConnectionResolverInterface;
use EasySwoole\EasySwoole\Config;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');

        /*****************  注册 hyperf-orm 连接池 *****/
        Di::getInstance()->set(ContainerInterface::class, Container::class);
        Di::getInstance()->set(ConfigInterface::class, ConfigFactory::class);
        Di::getInstance()->set(ConnectionResolverInterface::class,  ConnectionResolver::class, []);

        $databases = Config::getInstance()->getConf('databases');
        $manager = Manager::getInstance();
        foreach ($databases as $name => $conf) {
            if (!is_null($manager->get($name))) {
                continue;
            }
            Manager::getInstance()->register(new MysqlPool($conf), $name);
        }

        /*****************  注册 hyperf-orm 连接池结束 *******************/

        /*****************  注册 mysql orm 连接池 *****/
//        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
//        // 【可选操作】我们已经在 dev.php 中进行了配置
//        # $config->setMaxObjectNum(20); // 配置连接池最大数量
//        DbManager::getInstance()->addConnection(new Connection($config));
        /*****************  注册 mysql orm 连接池结束 *******************/
        /*****************  注册 session服务开始 ********************/
        // 可以自己实现一个标准的 session handler，下面使用组件内置实现的 session handler
        // 基于文件存储，传入 EASYSWOOLE_TEMP_DIR . '/Session' 目录作为 session 数据文件存储位置
        Session::getInstance(new FileSession(EASYSWOOLE_TEMP_DIR . '/Session'));
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_ON_REQUEST, function (Request $request, Response $response) {
            // TODO: 注册 HTTP_GLOBAL_ON_REQUEST 回调，相当于原来的 onRequest 事件
            // 获取客户端 Cookie 中 easy_session 参数
            $sessionId = $request->getCookieParams('easy_session');
            if (!$sessionId) {
                $sessionId = Random::character(32); // 生成 sessionId
                // 设置向客户端响应 Cookie 中 easy_session 参数
                $response->setCookie('easy_session', $sessionId);
            }
            // 存储 sessionId 方便调用，也可以通过其它方式存储
            $request->withAttribute('easy_session', $sessionId);
            Session::getInstance()->create($sessionId); // 创建并返回该 sessionId 的 context
        });
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (Request $request, Response $response) {
            // TODO: 注册 HTTP_GLOBAL_AFTER_REQUEST 回调，相当于原来的 afterRequest 事件
            // session 数据落地【必不可少这一步】
            Session::getInstance()->close($request->getAttribute('easy_session'));
            // gc 会清除所有 session，切勿操作
            // Session::getInstance()->gc(time());
        });
        /*****************  注册 session服务结束 ********************/
    }

    public static function mainServerCreate(EventRegister $register)
    {
        $config = new \EasySwoole\Socket\Config();
        $config->setType($config::WEB_SOCKET);
        $config->setParser(WebSocketParser::class);
        $dispatcher = new \EasySwoole\Socket\Dispatcher($config);
        $config->setOnExceptionHandler(function (\Swoole\Server $server, \Throwable $throwable, string $raw, \EasySwoole\Socket\Client\WebSocket $client, \EasySwoole\Socket\Bean\Response $response) {
            $response->setMessage('system error!');
            $response->setStatus($response::STATUS_RESPONSE_AND_CLOSE);
        });

// 自定义握手
        /*$websocketEvent = new WebSocketEvent();
        $register->set(EventRegister::onHandShake, function (\Swoole\Http\Request $request, \Swoole\Http\Response $response) use ($websocketEvent) {
            $websocketEvent->onHandShake($request, $response);
        });*/

        $register->set($register::onMessage, function (\Swoole\Websocket\Server $server, \Swoole\Websocket\Frame $frame) use ($dispatcher) {
//            $dispatcher->dispatch($server, $frame->data, $frame);
            $server->send($frame->fd,'this is message');
            $server->push($frame->fd,123);
        });
    }
}